<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Payment;
use App\File;
use App\User;

class Client extends Model
{
    protected  $fillable = [
        'first_name', 'last_name', 'email', 'address', 'state', 'time_zone', 'phone', 'phone2', 'email2', 'resort', 'bedroom',
        'week', 'price', 'agent_id', 'buyer', 'escrow', 'maintenance', 'transfer_fee_concept', 'escrow_agent', 'transfer_fee', 'contract_number', 'status_id', 'user_id', 'last_modified_user_id', 'favorites'
    ];
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
    public function notes()
    {
        return $this->hasMany(Note::class)->orderByDesc('id');
    }
    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function user_updated()
    {
        return $this->belongsTo(User::class, 'last_modified_user_id');
    }
    //RELACIONES ORDENADAS POR FECHA MAS RECIENTE DEL UPDATED AT
    public function notes_order_updated()
    {
        return $this->hasMany(Note::class)->orderByDesc('updated_at');
    }
    public function files_order_updated()
    {
        return $this->morphMany(File::class, 'fileable')->orderByDesc('updated_at');;
    }

    //guardamos el cliente directamente desde el form que recibimos
    public static function saveClient($client)
    {
        $user_id = auth('api')->user()->id;
        $exist = self::existingClient($client);
        if ($exist) return false;
        $client['user_id'] = $user_id;
        $client['last_modified_user_id'] = $user_id;
        $client_create = Client::create($client);
        $client_create->logical_delete = false;
        $payment =  new Payment();
        $payment->description = "";
        $payment->client_id = $client_create->id;
        $payment->user_id =  $user_id;
        $payment->last_modified_user_id = $user_id;
        $payment->save();
        $client_create->save();
        return $client_create;
    }
    public static function  existingClient($client)
    {
        $phone = preg_replace('/[^0-9]/', '', $client['phone']); //esto imprime de 444-333-222 a 444333222
        $phone2 = preg_replace('/[^0-9]/', '', $client['phone2']);

        $exist = false;
        $clients = Client::all();
        foreach ($clients as $key => $c) {
            if (($c->first_name == $client['first_name'] && $c->last_name  == $client['last_name'])) {
                $exist = true;
            }
            $dbPhone = preg_replace('/[^0-9]/', '', $c->phone);
            $dbPhone2 = preg_replace('/[^0-9]/', '', $c->phone2);

            if ($dbPhone == $phone && $dbPhone !== "") {
                $exist = true;
            }
            if ($dbPhone2 == $phone2 && $dbPhone2 !== "") {
                $exist = true;
            }
        }
        return $exist;
    }
    public static function updateClient($client, $id)
    {
        $user_id = auth('api')->user()->id;
        $client = $client['data'];
        $profile = Client::findOrFail($id);
        $profile->first_name = $client['first_name'];
        $profile->last_name = $client['last_name'];
        $profile->status_id = $client['status_id'];
        $profile->address = $client['address'];
        $profile->state = $client['state'];
        $profile->time_zone = $client['time_zone'];
        $profile->phone = $client['phone'];
        $profile->email = $client['email'];
        $profile->email2 = $client['email2'];
        $profile->phone2 = $client['phone2'];
        $profile->resort = $client['resort'];
        $profile->bedroom = $client['bedroom'];
        $profile->week = $client['week'];
        $profile->price = $client['price'];
        $profile->favorites = $client['favorites'];
        $profile->maintenance = $client['maintenance'];
        $profile->transfer_fee_concept = $client['transfer_fee_concept'];
        $profile->transfer_fee = $client['transfer_fee'];
        $profile->contract_number = $client['contract_number'];
        $profile->agent_id = $client['agent_id'];
        $profile->escrow = $client['escrow'];
        $profile->buyer  = $client['buyer'];
        $profile->escrow_agent  = $client['escrow_agent'];

        $profile->last_modified_user_id = $user_id;
        $profile->updated_at = date('Y-m-d H:i:s');
        $profile->save();
        return self::with(['files', 'status', 'notes'])->findOrFail($id);
    }
    public static function clientsConsult()
    {
        return Client::all();
    }
    public static function deleteClient($id)
    {

        $client = Client::findOrFail($id);
        $client->logical_delete = true;
        $client->save();
        return $id;
    }
    public static function restore($id)
    {

        $client = Client::findOrFail($id);
        $client->logical_delete = false;
        $client->save();
        return $id;
    }
    public static function deleteClientPermanently($id)
    {

        $client = Client::findOrFail($id);
        $payment = Payment::where('client_id', $client->id)->first();
        if ($payment) {
            $payment->delete();
        }
        $client->delete();
        return $id;
    }
    public static function getClient($id)
    {
        $client = self::with(['files', 'status', 'notes.user'])->findOrFail($id);
        $color = Status::findOrFail($client->status_id);
        return  response()->json([
            'client' => $client,

        ]);
    }


    public static function advancedSearch($search)
    {
        $user_id = auth('api')->user()->id;
        $status_id = $search['statusID'];
        $field = $search['field'];
        if ($search['show']) {
            $dateFrom = Carbon::parse($search['dateFrom'])->format('Y/m/d 00:00:00');
            $dateTo = Carbon::parse($search['dateTo'])->format('Y/m/d 23:59:00');
            $clients = Client::where('created_at', '>=', $dateFrom)
                ->where('created_at', '<=', $dateTo)
                ->when($status_id <> 0, function ($query) use ($status_id) {
                    return $query->where('status_id', $status_id);
                })
                ->where('logical_delete', '<>',  1)
                ->with([
                    'user_updated', 'status', 'files', 'notes',
                    'payments.user_updated', 'notes_order_updated.user_updated',
                    'files_order_updated.user_updated'
                ]);
            return  $clients->when($field <> "", function ($query) use ($field) {
                return $query->where('first_name', 'like', "%{$field}%")
                    ->orWhere('last_name', 'like', "%{$field}%")
                    ->orWhere('resort', 'like', "%{$field}%")
                    ->orWhere('phone', 'like', "%{$field}%")
                    ->orWhere('phone2', 'like', "%{$field}%");
            })
                ->orderBy('id', 'desc')
                ->get();
        } else {
            $clients =  Client::where('logical_delete', '<>',  1);
            $clients = $clients->where('first_name', 'like', "%{$search['field']}%")
                ->orWhere('last_name', 'like', "%{$search['field']}%")
                ->orWhere('resort', 'like', "%{$search['field']}%")
                ->orWhere('phone', 'like', "%{$search['field']}%")
                ->orWhere('phone2', 'like', "%{$search['field']}%")

                ->with(['user_updated', 'status', 'files', 'notes', 'payments.user_updated', 'notes_order_updated.user_updated', 'files_order_updated.user_updated'])
                ->orderBy('id', 'desc')

                ->get();


            return $clients;
        }
    }
    public static function dashboard()
    {
        $favorites = self::where('favorites', 1)
            ->with(['user_updated', 'status', 'files', 'notes', 'payments.user_updated', 'notes_order_updated.user_updated', 'files_order_updated.user_updated'])
            ->orderBy('id', 'desc')->get();


        return  response()->json([
            // 'statuses' => $statuses,
            'favorites' => $favorites

        ]);
    }
}
