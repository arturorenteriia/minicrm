<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'alias',
        'file_type_id',
        'disk_name',
        'file_name',
        'file_size',
        'file_path',
        'mime_type',
        'width',
        'height',
        'fileable_id',
        'fileable_type',
    ];
    public function user_updated()
    {
        return $this->belongsTo(User::class, 'last_modified_user_id');
    }

    public function fileable()
    {
        return $this->morphTo();
    }

    public function fileType()
    {
        return $this->belongsTo(FileType::class);
    }
}
