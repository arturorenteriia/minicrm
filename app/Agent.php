<?php

namespace App;
use App\Client;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    public function clients()
    {
        return $this->hasMany(Client::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
