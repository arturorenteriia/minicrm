<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
            return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'data.first_name' => ['required','max:100'],
            'data.last_name' => ['required', 'max:100'],
            'data.address' => ['nullable', 'sometimes', 'max:100'],
            'data.state' => ['nullable', 'sometimes', 'max:100'],
            'data.phone' => ['nullable', 'sometimes', 'max:15'],
            'data.phone2' => ['nullable', 'sometimes', 'max:15'],
            'data.email' => ['nullable','email', 'sometimes', 'max:100'],
            'data.email2' => ['nullable','email', 'sometimes', 'max:100'],
            'data.status_id' => 'exists:statuses,id',
            'data.price' =>  ['nullable', 'sometimes', 'numeric'],
            'data.transfer_fee' =>  ['nullable', 'sometimes', 'numeric'],


        ];
    }

    public function messages() {
        return [
            'data.phone.max' => 'Phone too long.',
            'data.phone2.max' => 'Phone 2 too long.',
            'data.contact_number.max' => 'Contact Number too long.',
            'data.email.email' => 'Email field must be a valid email address.',
            'data.email2.email' => 'Email 2 field must be a valid email address',
            'data.status_id.exists' => 'Status id  not valid' ,
            'data.price.numeric' => 'Price not valid, must be numeric',
            'data.transfer_fee.numeric' => 'Transfer fee not valid, must be numeric'
        ];

    }
}
