<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);
        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'], 401);
        }
        $user = $request->user();
       
        $tokenResult = $user->createToken('Personal Access Token');
       /* $saveUser = User::findorfail($user->id);
        $saveUser->api_token = $tokenResult->accessToken;
        $saveUser->save();*/

        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString(),
            'test' => 2
        ]);
    }
    public function logout(Request $request)
    {

        $request->user()->token()->revoke();
        //$request->user()->token()->delete();
        return response()->json(['message' =>
            'Successfully logged out']);
    }
}
