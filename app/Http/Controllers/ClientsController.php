<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Http\Requests\StoreClientForm;
use App\Status;
use Illuminate\Support\Facades\DB;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Client::with(['user_updated', 'status', 'files', 'notes', 'payments.user_updated', 'notes_order_updated.user_updated', 'files_order_updated.user_updated'])
            ->where('logical_delete', '<>',  1)
            ->orderBy('id', 'desc')->take(50)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientForm $request)
    {
        $response = Client::saveClient($request->all()['data']);
        if ($response == false) {
            return $this->failResponse([], 'A client with the same phone or name and last name are created already.');
        }
        return  $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Client::getClient($id);
    }

    public function advancedSearch(Request $request)
    {
        $response = Client::advancedSearch($request['data']);
        return $this->successResponse($response, 'Success');
    }
    public function favorites(Request $request)
    {
        $edit = Client::findOrFail($request->all()['id']);
        $edit->favorites = !$edit->favorites;
        $edit->save();
        return $edit->favorites;
    }
    public function deletePermanently(Request $request)
    {
        return Client::deleteClientPermanently($request->all()['id']);
    }
    public function restore(Request $request)
    {
        $response = Client::restore($request->all()['id']);
    }
    public function trash(Request $request)
    {
        return Client::with(['user_updated', 'status', 'files', 'notes', 'payments.user_updated', 'notes_order_updated.user_updated', 'files_order_updated.user_updated'])
            ->where('logical_delete',   1)
            ->orderBy('id', 'desc')->get();
    }
    public function getClientsByStatus(Request $request)
    {

        $status = $request['status'];
        $clients = Client::where('status_id', $status)->with(['user_updated', 'status', 'files', 'notes', 'payments.user_updated', 'notes_order_updated.user_updated', 'files_order_updated.user_updated'])
            ->where('logical_delete', '<>',  1)
            ->orderBy('id', 'desc')->take(50)->get();
        return $this->successResponse($clients, 'Success');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreClientForm $request, $id)
    {
        $response = Client::updateClient($request, $id);
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = Client::deleteClient($id);
    }

    public function dashboard()
    {
        $exclude = false;
        $user = auth('api')->user();
        if (!$user->hasRole('admin')) {
            $exclude = true;
        }

        $favorites = Client::where('favorites', 1)
            ->where('logical_delete', '<>',  1)
            ->with(['user_updated', 'status', 'files', 'notes', 'payments.user_updated', 'notes_order_updated.user_updated', 'files_order_updated.user_updated'])
            ->orderBy('id', 'desc')->get();

        $statuses = Status::select(['name', 'color_code', 'id', 'order'])
            ->when($exclude, function ($query) {
                return $query->where('name', '!=', 'cancelled');
            })
            ->orderBy('order')
            ->get();

        return  $this->successResponse(['favorites' => $favorites, 'statuses' => $statuses], 'Success dashboard');
    }
}
