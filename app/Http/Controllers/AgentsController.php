<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agent;
use App\Client;
class AgentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Agent::with(['user','clients'])->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agent = $request->all()['data'];
        $user_id = auth('api')->user()->id;
        $newAgent = new Agent();
        $newAgent->first_name = $agent['first_name'];
        $newAgent->last_name = $agent['last_name'];
        $newAgent->email = $agent['email'];
        $newAgent->phone = $agent['phone'];
        $newAgent->user_id = $user_id;
        $newAgent->last_modified_user_id = $user_id;
        $newAgent->save();
        return $this->successResponse([], 'Agent added correctly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = $request->all()['data'];
        $user_id = auth('api')->user()->id;
        $newAgent = Agent::findOrFail($id);
        $newAgent->first_name = $agent['first_name'];
        $newAgent->last_name = $agent['last_name'];
        $newAgent->email = $agent['email'];
        $newAgent->phone = $agent['phone'];
        $newAgent->user_id = $user_id;
        $newAgent->last_modified_user_id = $user_id;
        $newAgent->save();
        return $this->successResponse([], 'Agent updated correctly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clients = Client::where('agent_id', $id)->first();
        if ($clients) {
            return $this->failResponse([], 'This agent has clients related, cannot be deleted');
        }
        $agent = Agent::findOrFail($id);
        $agent->delete();
        return $this->successResponse([], 'Agent deleted correctly');
    }
}
