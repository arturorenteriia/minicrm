<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Status;
class StatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exclude = false;
        $user = auth('api')->user();
        if (!$user->hasRole('admin')) {
            $exclude = true;
        }

       $statuses = Status::select(['name', 'color_code', 'id', 'order'])
                    ->when($exclude, function($query) {
                        return $query->where('name', '!=', 'cancelled');
                    })
                    ->orderBy('order')
                    ->get();

       if ($statuses->count()) {
           return response()->json([
               'success' => true,
               'data' => $statuses
           ]);
        }

        return response()->json([
            'success' => false,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3|unique:statuses,name', 
            'color_code' => 'required|min:7|unique:statuses,color_code'
        ]);

        $status = Status::create($request->all());

        if ($status) {
            return $this->successResponse(['status' => $status], 'Status added successfully');
        }

        return $this->failResponse([], '¡Ups! someting went wrong');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:3|unique:statuses,name,'.$id, 
            'color_code' => 'required|min:7|unique:statuses,color_code,'.$id
        ]);

        $status = Status::findOrFail($id);
        $status->update($request->all());

        return $this->successResponse(['status' => $status], 'Updated category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Status::findOrFail($id);

        if (!$status->clients->count()) {
            $status->delete();

            return $this->successResponse([], 'Category removed');
        }

        return $this->failResponse([], 'Cant not delete this category because there are Clients with this status');
    }

    public function reorder(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        foreach ($data as $status) {
            Status::where('id', $status['id'])->update([
                'order' => $status['order']
            ]);
        }

        return $this->successResponse([], 'Categories updated');
    }
}
