<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function getDataUser()
    {
        $user = auth('api')->user();
        return $user->load('roles');
    }

    public function index()
    {
        $user = auth('api')->user();
        if (!$user->hasRole('admin')) {
            return $this->failResponse([], 'Unauthorized user');
        }
        $user = User::with('roles')->get();
        return $this->successResponse($user);
    }

    public function store(Request $request)
    {

        $user = auth('api')->user();
        if (!$user->hasRole('admin')) {
            return $this->failResponse([], 'Unauthorized user');
        }

        $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'role_id' => 'required|numeric',
            'email' => 'required|email|confirmed|unique:users,email',
            'password' => 'required|min:8|confirmed',            
        ]);

        $role = Role::findOrFail($request->role_id);
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $active_user = $this->getDataUser();
        $data['created_by'] = $active_user->name .  ' ' . $active_user->last_name;
        $data['updated_by'] = $active_user->name .  ' ' . $active_user->last_name;
        $user = User::create($data);
        $user->assignRole($role);

        return $this->successResponse([], 'User ' . $user->name . ' ' . $user->last_name . ' created');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return $this->successResponse($user);
    }

    public function update(Request $request, $id)
    {
        $user = auth('api')->user();
        if (!$user->hasRole('admin')) {
            return $this->failResponse([], 'Unauthorized user');
        }

        $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'role_id' => 'required|numeric',
            'email' => 'required|email|confirmed|unique:users,email,'.$id,
            'password' => 'nullable|min:8|confirmed',            
        ]);

        $data = $request->all();
        $user = User::findOrFail($id); 
        $role = Role::findOrFail($request->role_id);
        $user->syncRoles([$role->name]);    
        $active_user = $this->getDataUser();
        $data['updated_by'] = $active_user->name .  ' ' . $active_user->last_name;
        if (!empty($request->password)) {
            $data['password'] = bcrypt($data['password']);
        }
        $user->update($data);
        
        return $this->successResponse([], 'User ' . $user->name . ' ' . $user->last_name . ' updated successfully');
        
    }

    public function destroy($id)
    {
        $user = auth('api')->user();
        if (!$user->hasRole('admin')) {
            return $this->failResponse([], 'Unauthorized user');
        }
        
        $user = User::findOrFail($id);
        $user->delete();
        return $this->successResponse([], 'User ' . $user->name . ' was eliminated');
    }
}
