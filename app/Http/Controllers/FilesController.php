<?php

namespace App\Http\Controllers;

use App\Client;
use App\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'client_id' => 'required',
            'alias' => 'required',
            'file' => 'required'
        ]);

        $client = Client::findOrFail($request->client_id);
        $user_id = auth('api')->user()->id;
        $newFile = new File();
        $newFile->alias = $request->alias;
        $newFile->disk_name = 'public';
        $extension = $request->file->extension();
        $newFile->user_id = $user_id;
        $newFile->last_modified_user_id = $user_id;
        $file_name = explode('.', $request->file->getClientOriginalName());
        $newFile->file_name = Carbon::now()->format('Ymdhis_') . $file_name[0] . '.' . $extension;
        $newFile->file_size = $request->file->getSize();
        $folder = 'files/' . date('Y') . '/' .date('m');
        $newFile->file_path = '/' . $request->file->storeAs($folder, $newFile->file_name, $newFile->disk_name);
        $newFile->mime_type = $request->file->getClientMimeType();

        $file = $client->files()->save($newFile);
        if ($file) {
            return $this->successResponse(['file' => $file], 'File uploaded successfully');
         }

         return $this->failResponse();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = auth('api')->user()->id;
        $file = File::findOrFail($id);
        $file->last_modified_user_id = $user_id;
        $file->alias = $request->alias;
        $file->save();

        return $this->successResponse(['alias' =>$file->alias], 'File updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::findOrFail($id);
        $path = substr($file->file_path, 1);
        if (Storage::disk($file->disk_name)->exists($path)) {
            /** Eliminamos el archivo */
            Storage::disk($file->disk_name)->delete($path);
        }

        $file->delete();

        return $this->successResponse([],'File deleted');
    }
}
