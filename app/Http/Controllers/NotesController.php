<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Note;
use App\Status;
use App\Client;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth('api')->user()->id;
        $note = new Note();
        $note->note = $request->all()['note'];
        $note->client_id =$request->all()['id'];
        $note->user_id = $user_id;
        $note->last_modified_user_id = $user_id;
        $note->save();
        $allNotes = Note::where('client_id', $request->all()['id'] )->with(['user'])->orderByDesc('id')->get();
        return $this->successResponse([ 'notes' => $allNotes,], 'Note added correctly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);
        $notes = Note::where('client_id', $id)->get();
        $color = Status::where('id' ,$client->status_id )->select('color_code')->first();


        return $this->successResponse([
            'notes' => $notes,
            'color' => $color,
        ], 'Success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = auth('api')->user()->id;
        $note = Note::findOrFail($id);
        $newNote = $request->all()['note'];
        $note->note =  $request->all()['note'];
        $note->last_modified_user_id = $user_id;
        $note->updated_at = date('Y-m-d H:i:s');
        $note->save();

        return $this->successResponse([], 'Note updated correctly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);
        $client_id = $note->client_id;
        $note->delete();
        $notes = Note::where('client_id', $client_id)->orderByDesc('id')->get();

        return $this->successResponse([
            'notes'=> $notes
        ], 'Note deleted correctly');
    }
}
