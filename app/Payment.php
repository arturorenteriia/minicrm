<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function user_updated()
    {
        return $this->belongsTo(User::class, 'last_modified_user_id');
    }
    public static function updatePayment($description, $id)
    {
        $user_id = auth('api')->user()->id;
        $payment = Payment::where('client_id', $id)->first();
        $payment->description =  $description;
        $payment->last_modified_user_id = $user_id;
        $payment->updated_at = date('Y-m-d H:i:s');
        $payment->save();
        return "1";

    }
}
