<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = ['name', 'color', 'updated_by'];

    public function clients()
    {
        $this->belongsToMany(Client::class);
    }
}
