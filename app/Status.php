<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    protected $fillable = ['name', 'color_code', 'order'];
    
    public static function GetStatuses () {
        return DB::table('statuses')->select('name as label', 'id as value', 'color_code')->get();
    }

    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
