<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('address')->nullable();
            $table->string('state')->nullable();
            $table->string('time_zone')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->string('email')->nullable();
            $table->string('email2')->nullable();
            $table->string('resort')->nullable();
            $table->string('bedroom')->nullable();
            $table->string('week')->nullable();
            $table->double('price')->nullable();
            $table->boolean('favorites');
            $table->string('maintenance')->nullable();
            $table->string('transfer_fee_concept')->nullable();
            $table->double('transfer_fee')->nullable();
            $table->string('contract_number')->nullable();
            $table->unsignedBigInteger('status_id');
            $table->foreignId('user_id');
            $table->foreignId('last_modified_user_id');
            $table->timestamps();
           $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
