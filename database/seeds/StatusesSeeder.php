<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Status;
class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['New client', 'Buyer call', 'Escrow call', 'Resort call', 'Escrow closing', 'Paying', 'Paid'
        , '2nd charge', '3nd charge', 'Recue', 'Cancelled'];
        foreach ($statuses as $status) {
            Status::create([
                'name' => $status,
                'color_code' =>'#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT),             
            ]);
        }
      
    }
}
