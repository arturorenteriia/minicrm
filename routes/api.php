<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'auth'], function () {

    Route::post('login', 'Auth\AuthController@login');
    Route::post('signup', 'Auth\AuthController@signup');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'Auth\AuthController@logout');
        Route::get('user', 'Auth\AuthController@user');
        Route::get('getDataUser', 'UsersController@getDataUser');
        Route::get('users', 'UsersController@index');
        Route::get('users/{id}', 'UsersController@show');
        Route::post('users', 'UsersController@store');
        Route::put('users/{id}', 'UsersController@update');
        Route::delete('users/{id}', 'UsersController@destroy');

        Route::post('clients/search', 'ClientsController@advancedSearch');
        Route::post('clients/favorites', 'ClientsController@favorites');
        Route::post('clients/deletepermanently', 'ClientsController@deletePermanently');
        Route::post('clients/restore', 'ClientsController@restore');

        Route::get('clients/trash', 'ClientsController@trash');
        Route::post('clients/getClientsByStatus', 'ClientsController@getClientsByStatus');
        Route::get('clients/dashboard', 'ClientsController@dashboard');

        Route::post('statuses/reorder', 'StatusesController@reorder');

        Route::resources([
            'clients' => ClientsController::class,
            //'search' =>[ClientsController::class, 'advancedSearch'],
            'statuses' => StatusesController::class,
            'payments' => PaymentsController::class,
            'notes' => NotesController::class,
            'roles' => RolesController::class,
            'agents' => AgentsController::class,

        ]);
        Route::resource('files', 'FilesController');
    });
});
